#!/usr/bin/python

# Import the required modules
import cv2, os
import numpy as np
from PIL import Image
from flask import Flask, render_template

app = Flask(__name__)

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/train')
def get_images_and_labels(path):
    # Append all the absolute image paths in a list image_paths
    # We will not read the image with the .sad extension in the training set
    # Rather, we will use them to test our accuracy of the training
    image_paths = [os.path.join(path, f) for f in os.listdir(path)]
    # images will contain face images
    images = []
    # labels will contain the label that is assigned to the image
    labels = []
    # names will contain the names of the subjects
    names = {}
    for image_path in image_paths:
        try:
            # Read the image and convert to grayscale
            image_pil = Image.open(image_path).convert('L')
            # Convert the image format into numpy array
            image = np.array(image_pil, 'uint8')
            # Get the label of the image
            nbr = int(os.path.split(image_path)[1].split(".")[0].split("_")[1])
            # Get the name of the subject
            name = os.path.split(image_path)[1].split(".")[0].split("_")[0]
            num = int(os.path.split(image_path)[1].split(".")[0].split("_")[1])
            names[num] = name
            # Detect the face in the image
            faces = faceCascade.detectMultiScale(image)
            # If face is detected, append the face to images and the label to labels and append name in names
            for (x, y, w, h) in faces:
                images.append(image[y: y + h, x: x + w])
                labels.append(nbr)
                print "adding {}".format(name)
                #cv2.imshow(name, image[y: y + h, x: x + w])
                #cv2.waitKey(5)
        except:
            pass
    # return the images list and labels list
    return images, labels, names

@app.route('/livestream', methods=['POST', 'GET'])
def launch_livestream():
  # Path to the Yale Dataset
  path = './yalefaces'
  # Call the get_images_and_labels function and get the face images and the 
  # corresponding labels
  images, labels, names = get_images_and_labels(path)
  cv2.destroyAllWindows()
  
  # Perform the tranining
  recognizer.train(images, np.array(labels))
  
  # video feed
  cap = cv2.VideoCapture(0)
  
  while(True):
      # Capture frame-by-frame
      ret, frame = cap.read()
  
      # Our operations on the frame come here
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  
      cascPath = "haarcascade_frontalface_alt.xml"
      faceCascade = cv2.CascadeClassifier(cascPath)
  
      detected_faces = faceCascade.detectMultiScale(
          gray,
          scaleFactor=1.1,
          minNeighbors=6,
          minSize=(100, 100),
          flags=cv2.cv.CV_HAAR_SCALE_IMAGE
      )
  
      print "Found {0} faces!".format(len(detected_faces))
      #image_paths = [os.path.join(path, f) for f in os.listdir(path)]
      #predict_image = np.array(gray, 'uint8')
      for (x, y, w, h) in detected_faces:
          cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
          #predict_image = cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
          nbr_predicted, conf = recognizer.predict(gray[y: y + h, x: x + w])
          
          cv2.putText(frame, " {}  {}%".format(names[nbr_predicted], conf),(x - 4, y - 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 255, 0))
          #cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
          #cv2.waitKey(1000)
  
      # Display the resulting frame
      cv2.imshow('frame',frame)
      if cv2.waitKey(1) & 0xFF == ord('q'):
          break
  
  # When everything done, release the capture
  cap.release()
  cv2.destroyAllWindows()
 
@app.route('/video/<filename>', methods=['POST', 'GET']) 
def launch_video(filename):
  # Path to the Yale Dataset
  path = './yalefaces'
  # Call the get_images_and_labels function and get the face images and the 
  # corresponding labels
  images, labels, names = get_images_and_labels(path)
  cv2.destroyAllWindows()
  
  # Perform the tranining
  recognizer.train(images, np.array(labels))
  
  # video feed
  cap = cv2.VideoCapture(0)
  
  while(True):
      # Capture frame-by-frame
      ret, frame = cap.read()
  
      # Our operations on the frame come here
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  
      cascPath = "haarcascade_frontalface_alt.xml"
      faceCascade = cv2.CascadeClassifier(cascPath)
  
      detected_faces = faceCascade.detectMultiScale(
          gray,
          scaleFactor=1.1,
          minNeighbors=6,
          minSize=(100, 100),
          flags=cv2.cv.CV_HAAR_SCALE_IMAGE
      )
  
      print "Found {0} faces!".format(len(detected_faces))
      #image_paths = [os.path.join(path, f) for f in os.listdir(path)]
      #predict_image = np.array(gray, 'uint8')
      for (x, y, w, h) in detected_faces:
          cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
          #predict_image = cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
          nbr_predicted, conf = recognizer.predict(gray[y: y + h, x: x + w])
          
          cv2.putText(frame, " {}  {}%".format(names[nbr_predicted], conf),(x - 4, y - 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 255, 0))
          #cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
          #cv2.waitKey(1000)
  
      # Display the resulting frame
      cv2.imshow('frame',frame)
      if cv2.waitKey(1) & 0xFF == ord('q'):
          break
  
  # When everything done, release the capture
  cap.release()
  cv2.destroyAllWindows()

@app.route('/images', methods=['POST', 'GET'])
def launch_photos():
  # Path to the Yale Dataset
  path = './yalefaces'
  # Call the get_images_and_labels function and get the face images and the
  # corresponding labels
  images, labels = get_images_and_labels(path)
  cv2.destroyAllWindows()
  
  # Perform the tranining
  recognizer.train(images, np.array(labels))
  
  # Append the images with the extension .sad into image_paths
  image_paths = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.sad')]
  for image_path in image_paths:
      predict_image_pil = Image.open(image_path).convert('L')
      predict_image = np.array(predict_image_pil, 'uint8')
      faces = faceCascade.detectMultiScale(predict_image)
      for (x, y, w, h) in faces:
          nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
          nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("subject", ""))
          if nbr_actual == nbr_predicted:
              print "{} is Correctly Recognized with confidence {}".format(nbr_actual, conf)
          else:
              print "{} is Incorrect Recognized as {}".format(nbr_actual, nbr_predicted)
          cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
          cv2.waitKey(1000)
  
if __name__ == '__main__':
  #app.run(debug=True)
  app.run()
