# Documentation

## Train It
This allow your machine to learn how to recognize your face for futher use directly in the web browser, it is suppose to enable the camera through a Javascript code right [here](https://davidwalsh.name/browser-camera)

## Upload Photo
You can use this as another way to teach the machine how to recognize your face with direct upload of a set of photo.

## Upload Video
This will use the same functionality as with the photo upload but with a videos format which enable better and easier shot to be made.

## Start LiveStream
You enable a video flux from your computer to the server to send data and enable the machine to learn in a better way as it will be able to give different instruction to take picture from various angle.
